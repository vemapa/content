---
title: "Git-ohjeita"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: yes
    code_folding: show
---


<iframe width="560" height="315" src="https://www.youtube.com/embed/qv_UKAoCwN4" frameborder="0" allowfullscreen></iframe>

Mikäli A) käytät R:ää tai muuta *skriptattavaa* (vs. valikkopohjainen) ohjelmaa datan analysoimisessa, ja kirjoitat B) analyysit tekstimuotoisiin (plain text) tiedostoihin on versiohallinnan (revision control / version control) käyttöönotto on erinomainen idea.

**Miksi?**

- oman työn ja tiedonhallinnan "kestävyys" (sustainable software and data management practises)
- oppiminen ja kontribuoiminen tutkimusprojekteihin ja tutkimusohjelmistoihin
- tieteen avoimuus
    - datat & analyysit (julkaisut jo on..)
    - analyysien (ohjelmistojen) tekeminen viitattaviksi
    - datojen dokumentointi, lisensointi ja esillepano


- [TIE-02200 Ohjelmoinnin peruskurssi S2015: Git ja versionhallinta](http://www.cs.tut.fi/~opersk/S2015/@wrapper.shtml?materiaalit/git)

![](https://s-media-cache-ak0.pinimg.com/originals/a2/ce/7d/a2ce7d2d2c0d1fdbf36914b844707f1a.gif)

## Meidän monimutkainen git-infrastruktuuri

![Hahmotelma siitä, mikä on upstream (utur2016/content), mikä origin (sinun_utu_tunnus/content) ja mikä paikallinen kone (omakone tai csc:n poutapilvi)](http://i.stack.imgur.com/LtFGa.png)


## Oppaita

- <https://www.youtube.com/watch?v=U8GBXvdmHT4> - aloita tästä 54 minuuttia
- <https://guides.github.com/>
- <https://www.youtube.com/githubguides>
- <https://git-scm.com/book/fi/v1/Alkusanat-Versionhallinnasta>
* <http://happygitwithr.com>
* <https://beta.rstudioconnect.com/jennybc/happy-git-with-r/>

