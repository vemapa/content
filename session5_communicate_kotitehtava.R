---
title: "Veskun ja Niklaksen R-kurssin loppuraportti"
author: "vemapa@utu.fi; nisasa@utu.fi"
date: "10 marraskuuta 2016"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

http://rpubs.com/vemapa/kotitehtava5

## Suuri Liedon lautakunta-analyysi

Tutkimme Liedon lautakuntien välisiä eroja. Aineistona käytimme Liedon ostolaskuja vuonna 2015, aineisto saatavilla: http://opendata.lounaistieto.fi/aineistoja/Ostolaskut_Lieto_2015.xlsx.

Tarkoituksenamme on harjoitella tidyverse paketin käyttöä ja kuvailevien menetelmien soveltamista.

Datan laataamisessa oli ongelmia, joten päädyimme tähän dataan. Tarkoitus oli tehdä lisääkin esimerkiksi datan järjestämistä eri tavoin, mutta aika loppui kesken.

Teimme loppuraportin parityönä ja ajattelimme, että on järkevintä palauttaa myös yhtenä kappaleena.

Kurssi oli haastava noviisille, mutta kuitenkin innostava. Ennen kaikkea hyvä tietää jotain ohjelmistoympäristöstä ja mistä tietoa on saatavilla. Kiitos!

(Kiitämme kurssikaveria "majlaan" tuesta koko kurssin ajalta.)  


Seuraavaksi analyysin vaiheet.

## 1. Datan haku

Haetaan data 


```
download.file("http://opendata.lounaistieto.fi/aineistoja/Ostolaskut_Lieto_2015.xlsx", mode="wb", destfile = "C:/Users/Vesa-Matti/Documents") 

Ei onnistu lataaminen jostain`--> 'Permission denied

Ladataan suoraan koneelle ja avataan excel-tiedosto

```{r}
library(tidyverse)
setwd("C:/Users/Vesa-Matti/Desktop/")
lieto <- readxl::read_excel("Ostolaskut_Lieto_2015.xlsx")
print(lieto)


```

Data ei vaadi siivoamista, joten mennään suoraan käppyröihin. 

## 2. Kuvailevat menetelmät

### 2.1 Histogrammi

Halutaan histogrammi-kuvio, missä "lautakunnat" on x akselilla ja "tiliöintisumma" y akselilla.

```{r}

library(tidyverse) 
library(ggplot2)
p3 <- ggplot(data=lieto, aes(x=Lautakunta, y=Tiliöintisumma)) 
p3 <- p3 + geom_bar(stat="identity")
p3 <- p3 + coord_flip()
p3 <- p3 + labs(x="Lautakunta", y="summa euroissa")
p3 <- p3 + labs(title="Liedon kunnan eri lautakuntien tiliöintisummien erot 2015")
p3 

```

Lautakuntien nimet menivät päällekkäin, mutta komennlla + coord_flip() homma rullaa. 

Euromäärä ei havainnollistava (summat kuviossa epämääräisinä potensseina), kokellaan komentoa p3 <- p3 + scale_y_continuous(labels = comma), palataan asiaan myöhemmin


### 2.2 Piirakkakuvio

Harjoitellaan piirakkakuvion tekemistä sekä dplyr-paketin käyttöä

Luotiin uusi data filtteröimällä kolme euromääräisesti pienintä Lautakuntaa pois.

```{r}
lieto2 <- lieto %>% 
dplyr :: filter(Lautakunta %in% c("Kaavoitus- ja rakennuslautakunta","Kulttuuri- ja vapaa-aikalautakunta","Kunnanhallitus","Sosiaali- ja terveyslautakunta","Tekninen lautakunta","Varhaiskasvatus- ja koulutuslautakunta"))

```

Tehdään piirakkakuvio, missä muuttujat "Lautakunta" ja "Tiliöintisumma"

```{r}


pie <- ggplot(lieto2, aes(x = factor(1), fill = factor(Lautakunta))) +
  geom_bar(width = 1)
pie + coord_polar(theta = "y")

```

Edellinen pylväskuvio osoittaa, että Sosiaali- ja terveyslautakunnanlla olivat suurimmat menot. Piirakkakuvio havainnollistaa puolestaan, että teknisellä lautakunnalla ostojen lukumäärä on selvästi suurin, noin puolet kaikista ostoista.

### 2.3 Lautakuntien menot

Seuraavaksi tehdään taulukko, jossa lautakuntien käytetyt summat yhteenlaskettuna. Luodaan uusi muuttuja "kumumenosumma", ja laskettiin kaikkein menojen kumulatiivinen yhteissumma

```{r}

lieto <- lieto %>%
mutate(kumumenosumma = cumsum(Tiliöintisumma)) %>%
  print(lieto)
```

Aluksi tarkoitus oli taulukoida lautakunnittaisia kokonaismenoja, mutta "kumumenosumma" muuttujalle ei saatu käyttöarvoa.

Kokeillaan toista tapaa järjestää dataa.

Seuraavaksi järjestetään menot lautakunnittain dataan, eli jokaisen lautakunnan yhteenlasketut menot.

```{r}
lieto3 <- lieto %>%
group_by(Lautakunta) %>%
mutate(Lautakuntamenot = sum(Tiliöintisumma)) %>% 
select(Lautakunta,Lautakuntamenot) %>%
print(lieto3)


```

Saadaan selville, että seitsemän suurinta lautakuntaa (euroa):

1. Sosiaali- ja terveysl.         = 37.6 miljoonaa euroa
2. Tekninen laut.                 = 13.4 miljoonaa euroa
3. Varhaiskasvatus- ja koulutusl. = 4.7 miljoonaa euroa
4. Kunnanhallitus                 = 4.2 miljoonaa euroa
5. Kaavoitus- ja rakennusl.       = 1.6 miljoonaa euroa
6. Kulttuuri- ja vapaa-aikal.     = 0.8 miljoonaa euroa
7. Ympäristöl.                    = 0,2 miljoonaa euroa

Pylväskuviossa summat olivat potenssilukuina, mikä vaikeutti tarkkojen euromäärien havinnollistamista. Näin ollen oheisesta listasta saa paremman käsityksen asiasta. 

Ympäristölautakunnalle soisi kyllä vähän enemmänkin menoja...

Loppukaneetti:

Lieto - "Täällä on kaikki"
