---
title: "Datan visualisoiminen ja mallintaminen"
output: 
  html_document: 
    toc: true
    toc_float: true
    number_sections: yes
    code_folding: show
---



# Datan visualisointi ggplot2-paketilla

![](http://courses.markuskainu.fi/utur2016/kuviot/wickham_cycle.png)

[ggplot2](http://ggplot2.org/)-paketti perustuu [Leland Wilkinsonin](https://en.wikipedia.org/wiki/Leland_Wilkinson) ajatukseen *tilastografiikan kieliopista*, jonka hän hahmotteli kirjassaan [Wilkinson, Leland, ja Graham Wills. 2005. The grammar of graphics. Springer.](https://www.cs.uic.edu/~wilkinson/TheGrammarOfGraphics/GOG.html)

Grammar of graphics -ajattelun ytimessä on ajatus siitä, että minkä tahansa graafin voi tehdä samoilla peruselementeillä: tarvitaan *data*, *geometrioita* (`geoms`) eli datapisteitä merkitseviä visuaalisia elementtejä sekä *koordinaattijärjestelmä*.

- <http://docs.ggplot2.org/current/> - versio 2.2 tulee lähiviikkoina [link](http://koti.kapsi.fi/~muuankarski/fpa/elinolot2016/johdanto_kuvat.html)
- <http://www.cookbook-r.com/Graphs/>
- <http://r4ds.had.co.nz/graphics-for-communication.html>


## Perusteet

Tärkeintä että data *tidy*-muodossa! Sitten kaikki on mahdollista!


```{r, eval=FALSE}
library(tidyverse)


d <- mtcars
d$model <- row.names(d)
d$brand <- gsub("\\s.+$","",d$model)
d <- as_tibble(d)

# Pisteet geom_point()



# Viivat geom_line()



# Tolpat geom_bar()



# Tilastolliset jakaumaa kuvaavat graafit

## geom_histogram()


## geom_density()


## geom_boxplot()




```



## Paneelit ie. facets

```{r}

```


## Faktorit

- <http://forcats.tidyverse.org/>


Factors are used to describe categorical variables with a fixed and known set of levels. You can create factors with the base factor() or readr::parse_factor():

```{r, eval=FALSE}
library(forcats)

x1 <- c("Dec", "Apr", "Jan", "Mar")
month_levels <- c(
  "Jan", "Feb", "Mar", "Apr", "May", "Jun", 
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
)
factor(x1, month_levels)
readr::parse_factor(x1, month_levels)
```


The advantage of `parse_factor()` is that it will generate a warning if values of `x` are not valid levels:

```{r, eval=FALSE}
x2 <- c("Dec", "Apr", "Jam", "Mar")

factor(x2, month_levels)

parse_factor(x2, month_levels)
```


```{r, eval=FALSE}
d <- mtcars
d$model <- row.names(d)
d$brand <- gsub("\\s.+$","",d$model)
d <- as_tibble(d)

ggplot(d, aes(x=model,y=mpg,group=brand)) + 
  geom_point() +
  theme(axis.text = element_text(angle=90))

d$model <- fct_inorder(d$model) #
ggplot(d, aes(x=model,y=mpg,group=brand)) + 
  geom_point() + 
  theme(axis.text = element_text(angle=90))

d$brand <- fct_infreq(d$brand) #
ggplot(d, aes(x=brand,y=mpg,group=brand)) + 
  geom_point() + 
  theme(axis.text = element_text(angle=90))


# relevel
f <- factor(c("a", "b", "c"))
fct_relevel(f)
fct_relevel(f, "c")
fct_relevel(f, "b", "a")

# reverse
fct_rev(f)

# reorder
fct_reorder()
d$model <- fct_reorder(d$model, d$mpg) #
ggplot(d, aes(x=model,y=mpg,group=brand)) + 
  geom_point() + 
  theme(axis.text = element_text(angle=90))
```


## theme

The theming system in ggplot2 enables a user to control non-data elements of a ggplot object. It is composed of the following:

- theme elements, which refer to individual attributes of a graphic that are independent of the data, such as font size, axis ticks, appearance of grid lines or background color of a legend;
- theme element functions, which enables you to modify the settings of certain theme elements;
- theme functions, which define the settings of a collection of theme elements for the purpose of creating a specific style of graphics production;
- the theme() function, used to locally modify one or more theme elements in a specific ggplot object.


```{r}
theme()
```

## Hyviä tapoja kirjoittaa kuvien koodia

```{r}
# Tapa A
## Käsittele data
d <- mtcars
d$brand <- row.names(d)
d <- as_tibble(d)

## Luo kuvio kerros kerrokselta
p <- ggplot()
p <- p + geom_point(data=d, aes(x=brand,y=mpg))

# Tapa B

d <- readRDS(gzcon(url("http://courses.markuskainu.fi/utur2016/database/malesdata.RDS")))

d %>% group_by(year) %>% 
  summarise(mdw = median(wage, na.rm=TRUE),
            mw = mean(wage, na.rm=TRUE)) %>% 
  # Kuva alkaa - koska data tulee yo. prosessista, merkitään sitä vain pisteellä (.)
  ggplot(., aes(x=year,y=mw)) + geom_point()
```



## Skaalat

##  Erilaiset koordinaattijärjestelmät

### coord_polar() - pie charts


### Kartat

**Suomi**

- <https://github.com/rOpenGov/gisfin>
- <https://github.com/rOpenGov/pxweb/>


**Eurooppa & Eurostat**

- <https://github.com/rOpenGov/eurostat/blob/master/vignettes/eurostat_tutorial.md#maps>
- <http://ropengov.github.io/r/2015/05/01/eurostat-package-examples/>


## Lisäteemat ja ggplot2-paketin laajentaminen

- <http://r4ds.had.co.nz/graphics-for-communication.html#themes>
- ggthemes-paketti: <https://cran.r-project.org/web/packages/ggthemes/vignettes/ggthemes.html>
- ggthemr-paketti: <http://www.shanelynn.ie/themes-and-colours-for-r-ggplots-with-ggthemr/>
- ggplot2 themes <http://docs.ggplot2.org/dev/vignettes/themes.html>
- Extending ggplot2 <http://docs.ggplot2.org/dev/vignettes/extending-ggplot2.html>

![](http://r4ds.had.co.nz/images/visualization-themes.png)

**Linkkejä ggplot2:n laajennoksia**


+ [ggthemes](https://github.com/jrnold/ggthemes) - plot style themes
+ [ggmap](https://github.com/dkahle/ggmap) - maps with Google Maps, Open Street Maps, etc.
+ [ggiraph](http://davidgohel.github.io/ggiraph/introduction.html) - interactive ggplots
+ [ggstance](https://github.com/lionel-/ggstance) - horizontal versions of common plots
+ [GGally](https://github.com/ggobi/ggally) - scatterplot matrices
+ [ggalt](https://github.com/hrbrmstr/ggalt) - additional coordinate systems, geoms, etc.
+ [ggforce](https://github.com/thomasp85/ggforce) - additional geoms, etc.
+ [ggrepel](https://github.com/slowkow/ggrepel) - prevent plot labels from overlapping
+ [ggraph](https://github.com/thomasp85/ggraph) - graphs, networks, trees and more
+ [ggpmisc](https://cran.rstudio.com/web/packages/ggpmisc/) - photo-biology related extensions
+ [geomnet](https://github.com/sctyner/geomnet) - network visualization
+ [ggExtra](https://github.com/daattali/ggExtra) - marginal histograms for a plot
+ [gganimate](https://github.com/dgrtwo/gganimate) - animations
+ [plotROC](https://github.com/sachsmc/plotROC) - interactive ROC plots
+ [ggspectra](https://cran.rstudio.com/web/packages/ggspectra/) - tools for plotting light spectra
+ [ggnetwork](https://github.com/briatte/ggnetwork) - geoms to plot networks
+ [ggtech](https://github.com/ricardo-bion/ggtech) - style themes for plots
+ [ggradar](https://github.com/ricardo-bion/ggradar) - radar charts
+ [ggTimeSeries](https://github.com/Ather-Energy/ggTimeSeries) - time series visualizations
+ [ggtree](https://bioconductor.org/packages/release/bioc/html/ggtree.html) - tree visualizations
+ [ggseas](https://github.com/ellisp/ggseas) - seasonal adjustment tools



# Datan mallintaminen ja mallien hallinta

- <http://r4ds.had.co.nz/model-intro.html>
